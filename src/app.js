// @flow

let vec = [1, 2, 3, 4];

console.log('2 * vec = ', vec.map(e => e * 2));

let vec1 = [1, 2, 3];
let vec2 = [4, 5, 6];

//Oppgave 1
console.log('\nExercise 1: ','\n');
console.log('2+vec1 = ', vec1.map(e => e+2));
console.log('2*vec1 = ', vec1.map(e => e*2));

let average = vec1.reduce((acc,e) => acc+e);
console.log('Vec1 mean ', average/vec1.length);

let counter = -1;
let dotVec = vec1.map(e=>{
    counter++;
    return e*vec2[counter];
});
console.log('vec1 dot vec2 ', dotVec.reduce((acc,e) => acc+e));

let sumOfVec1 = vec1.reduce((acc,e) => acc+e);
let sumOfVec2X2 = vec2.map(e => e*2).reduce((acc,e) => acc+e);
console.log(sumOfVec2X2);
console.log('sum of vec1 + 2*vec2 ',sumOfVec2X2+sumOfVec1);

let vec1String: string = vec1.map(e => e.toString());
console.log('Vec1 as String ', vec1String);

//Oppgave 2
console.log('\nExercise 2: ','\n');
//Given class
class Complex {
    real: number;
    imag: number;

    constructor(real: number, img: number) {
        this.real = real;
        this.imag = img;
    }//end constructor
}//end class

let v = [new Complex(2, 2), new Complex(1, 1)];

let vElementsAsStrings: string = v.map(e=>e.real.toString()+' + ' + e.imag.toString()+'i');
console.log('V elements as string ', vElementsAsStrings);

let magnitudeOfV = v.map(e => Math.sqrt(e.real*e.real+e.imag*e.imag));
console.log('Magnitude of v elements ',magnitudeOfV);

//Can be chained (v.map(...).reduce(...))
let vReal = v.map(e=>e.real);
let vImag = v.map(e=>e.imag);
let sumOfV = new Complex(vReal.reduce((acc,e) =>acc+e), vImag.reduce((acc,e) => acc+e));
console.log('Sum of v ', sumOfV);

//Oppgave 3
console.log('\nExercise 3: ','\n');
//Given
let students = [{ name: 'Ola', grade: 'A' }, { name: 'Kari', grade: 'C' }, { name: 'Knut', grade: 'C' }];

let studentElementsString: string = students.map(e => e.name + ' got ' + e.grade);
console.log('Student elements as string ', studentElementsString);
let studentsGotC = students.filter(e => e.grade.toString() === 'C').length;
console.log('Number of students with C is ', studentsGotC);

let percentageC = studentsGotC/students.length;
console.log('Percentage of C grades is ', percentageC);

console.log('Someone got A? ', students.some(e => e.grade.toString() === 'A'));
console.log('Someone got F? ', students.some(e => e.grade.toString() === 'F'));

